## Helpers

game = -> Session.get 'game'

## Template Data and Events

Template.page.title = 'Tic Tac Toe'

Template.game.game = -> game()

Template.game.range = (number) -> _.range number

Template.game.cellIndex = (game, row, column) ->
  game.board.height*row + column

Template.game.cellValue = (game, row, column) ->
  game.board.cells[Template.game.cellIndex.apply null, arguments]

Template.game.disabled = (game, row, column) ->
  if game.winner or ' ' != Template.game.cellValue.apply null, arguments
    return 'disabled'
  return ''

Template.game.events(
  'click .square:not(.disabled)': (event) ->
    # Data attributes are slow slow slow
    index = parseInt event.toElement.dataset.boardIndex, 10
    Session.set 'game', game_over make_move game(), index
  'click #play-again-button': (event) ->
    Session.set 'game', new_game()
)

Meteor.startup ->
  if Meteor.isClient
    Session.set 'game', new_game()
