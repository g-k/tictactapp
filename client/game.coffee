random_choice = (choices) ->
  choices[Math.floor Math.random() * choices.length]

new_game = (width=3, height=3) ->
  return {
    moves: []
    to_move: random_choice ['X', 'O']
    board:
      width: width
      height: height
      cells: (' ' for i in _.range width * height)
    # when won:
    # winner: X or O
    # winning_sequences: [] # List of tictactoes for winner
  }

opponent = (player) -> { X: 'O', O: 'X' }[player]

make_move = (game, index) ->
  # Fail if no game or player
  if not game
    throw new Meteor.Error 400, 'Bad Request', "No game to make move."

  # Fail if game is over
  if game.winner
    throw new Meteor.Error 403, 'Forbidden', "Game over man."

  # Fail if spot is taken
  if game.board.cells[index.toString()] != ' '
    throw new Meteor.Error 403, 'Forbidden', "Can't move to filled space."

  # Make the move
  game.board.cells[index] = game.to_move
  game.moves.push index

  # Now it's the other player's turn
  game.to_move = opponent game.to_move

  return game


WINNING_INDICES = [
  # diagonals
  [0, 4, 8], # from top left
  [2, 4, 6], # from top right

  # rows
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],

  # columns
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
]


game_over = (game) ->
  # Game Over if all of a row or column or main diagonal/trace filled
  # with the same mark
  # Returns winning indices or false
  {board} = game

  # Lists of ways won
  ways_won = []
  for combo in WINNING_INDICES
    for mark in ['X', 'O']
      if _.all (board.cells[index] == mark for index in combo)
        ways_won.push combo

  out_of_moves = (game) ->
    # it's nobody's turn
    game.to_move = null
    delete game.to_move
    return game

  # if the game is over
  if ways_won.length
    # Someone won
    # Note: we've already registered the move
    game.winner = opponent game.to_move
    game = out_of_moves game
  else if _.all (cell != ' ' for cell in board.cells)
    game.winner = 'Nobody'
    game = out_of_moves game

  return game
