# Tic Tac Toe

When the player first visits
  generate a username (don't require login/password/etc. but later allow it to be entered)
  start a tic tac toe game against a free player

When a player moves
  register move on board
  if game over
    cross out board
    show postgame (with countdown to start another game)
  else
    update game to for next turn (other player's move)
